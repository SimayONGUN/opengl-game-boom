/////////////////////////////////////////////////////////         

///////////////////////////////////////////////////////// 

#include <vector>
#include <cmath>
#include <iostream>
#include <time.h>   
#ifdef __APPLE__
#  include <GLUT/glut.h>
#else
#  include <GL/glut.h>
#endif

using namespace std;

struct THING {
	float speedX;
	float speedY;
	float locationX;
	float locationY;
	int level;
	bool exists; // if a bomb collides with this thing, attribute 'exists' is 0, and the color of this thing is black; otherwise 'exists' is 1, and color of this thing is yellow or purple
	bool deleteThing; // if this thing stayed as black color enough, 'deleteThing' is 1, and the application will not draw this thing anymore
};

struct BOMB {
	float centerX;
	float centerY;
	float level;
};

void bitMapString(float x, float y, char s[]);

// Globals.
static GLsizei width, height; // OpenGL window size.
static int leftMouseButtonClicked = 0;  //if left mouse button is clicked, this variable is not equals to 0 mod2.
static THING things[5][4];
std::vector<BOMB> bombList;
static bool paused = false; 
static int s_pressed = 0; 

void gameOverFunction() { // this function checks if all the evil things has died, and terminates the program if all the evil things has died
	int exit_ = 0;
	for (int i = 3; i < 5; i++) {
		for (int j = 0; j < 4; j++) {
			if (things[i][j].exists == 0)
				exit_++;
		}
	}
	if (exit_ == 8) {
		exit(0);
	}
		


}

int calculatePoint() { // this function calculate the point
	int point = 0;
	int exit_ = 0;
	for (int i = 3; i < 5; i++) {
		for (int j = 0; j < 4; j++) {
			if (things[i][j].exists == 0)
				exit_ += (i - 2);
		}
	}

	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 4; j++) {
			if (things[i][j].exists == 0)
				if (i == 0)
					exit_ -= 4;
				else if (i == 1)
					exit_ -= 2;
				else
					exit_ -= 1;

		}
	}
	point += exit_ * 1000;
	return point;
}



void createThings() { // this function creates things with random positions, and random speeds of random directions

	int i, j;
	float r1, r2, r3, r4, r5, r6;
	for (i = 0; i < 5; i++) {
		for (j = 0; j < 4; j++) {
			r1 = rand() % 10 + 1; //thing'in x eksenindeki h�z bile�eni i�in random say�
			r2 = rand() % 10 + 1; //thing'in y eksenindeki h�z bile�eni i�in random say�
			r3 = rand() % 400 + 1; //thing'in ba�lang�� konumunun x koordinat� i�in random say�
			r4 = rand() % 400 + 1; //thing'in ba�lang�� konumunun y koordinat� i�in random say�
			r5 = rand() % 10 + 1; //thing'in x eksenindeki h�z bile�eninin eksi mi art� m� oldu�unu belirlemek i�in random say�
			r6 = rand() % 10 + 1; //thing'in y eksenindeki h�z bile�eninin eksi mi art� m� oldu�unu belirlemek i�in random say�
			if (r5 > 5) //e�er r5 5'ten b�y�kse thing'in x eksenindeki h�z� eksi y�nde olsun
				r1 = -1 * r1;
			if (r6 > 5) //e�er r6 5'ten b�y�kse thing'in y eksenindeki h�z� eksi y�nde olsun
				r2 = -1 * r2;
			if (r3 > 380)
				r3 = 380;
			if (r4 > 380)
				r4 = 380;
			things[i][j].speedX = r1*0.3;
			things[i][j].speedY = r2*0.3;
			things[i][j].locationX = r3;
			things[i][j].locationY = r4;
			things[i][j].level = i;
			things[i][j].exists = 1;
		}
	}
}

void bitMapString(float x, float y, char s[]) { // this function draws text on window
	int i = 0;
	glRasterPos2f(x, y);
	while (s[i] != '\0') {
		glutBitmapCharacter(GLUT_BITMAP_8_BY_13, s[i]);
		++i;
	}
}
// Drawing routine.
void drawScene(void)
{

	glClear(GL_COLOR_BUFFER_BIT);
	// Define viewport.
	glViewport(0, 0, width, height);

	for (int i = 0; i < 5; i++) {
		for (int j = 0; j < 4; j++) {
			if (!things[i][j].deleteThing) { //if the thing has not disappeared draw the thing with its color
				if (things[i][j].level < 3) {
					if (things[i][j].level == 0) {
						glColor3f(0.7*things[i][j].exists, 0.6 *things[i][j].exists, 0); // color of the level 0. if a bomb hit i,jth thing, draw the thing black
					}
					else if (things[i][j].level == 1) {
						glColor3f(0.9*things[i][j].exists, 0.8 *things[i][j].exists, 0); // color of the level 1. if a bomb hit i,jth thing, draw the thing black
					}
					else {
						glColor3f(1 * things[i][j].exists, 1 * things[i][j].exists, 0); // color of the level 2. if a bomb hit i,jth thing, draw the thing black
					}
				}
				else if (things[i][j].level == 3)
					glColor3f((0.3)*things[i][j].exists, 0, (0.5)*things[i][j].exists); // color of the level 3. if a bomb hit i,jth thing, draw the thing black
				else
					glColor3f((0.6)*things[i][j].exists, 0, (0.8)*things[i][j].exists); // color of the level 4. if a bomb hit i,jth thing, draw the thing black
				glBegin(GL_POLYGON);
				glVertex2f(things[i][j].locationX - 20, things[i][j].locationY + 20); 
				glVertex2f(things[i][j].locationX + 20, things[i][j].locationY + 20);
				glVertex2f(things[i][j].locationX + 20, things[i][j].locationY - 20);
				glVertex2f(things[i][j].locationX - 20, things[i][j].locationY - 20);

				if (things[i][j].locationX + 20 > 400) { //if the thing hit the right wall, its x speed is negated
					things[i][j].speedX *= -1;

				}
				if (things[i][j].locationX - 20 < 0 && things[i][j].speedX < 0) { //if the thing hit the left wall, its x speed is negated
					things[i][j].speedX *= -1;
				}
				if (things[i][j].locationY + 20 > 400) { //if the thing hit the top wall, its y speed is negated
					things[i][j].speedY *= -1;
				}
				if (things[i][j].locationY - 20 < 0 && things[i][j].speedY < 0) { //if the thing hit the bottom wall, its y speed is negated
					things[i][j].speedY *= -1;
				}
				glEnd();

			}

		}
	}



	for (int i = 0; i < bombList.size(); i++) { // draw the bomb. if the bomb is at a higher level, draw it smaller
		glColor3f(0.3 + i*0.3, 0.3, 0.3);
		glBegin(GL_POLYGON);
		glVertex2f(bombList.at(i).centerX + 10 - bombList.at(i).level, bombList.at(i).centerY + 10 - bombList.at(i).level); 
		glVertex2f(bombList.at(i).centerX + 10 - bombList.at(i).level, bombList.at(i).centerY - 10 + bombList.at(i).level);
		glVertex2f(bombList.at(i).centerX - 10 + bombList.at(i).level, bombList.at(i).centerY - 10 + bombList.at(i).level);
		glVertex2f(bombList.at(i).centerX - 10 + bombList.at(i).level, bombList.at(i).centerY + 10 - bombList.at(i).level);
		glEnd();
	}

	for (int k = 0; k < bombList.size(); k++) { //collusion control (bomb-things)
		for (int i = 0; i < 5; i++) {
			for (int j = 0; j < 4; j++) {
				if (bombList.at(k).level == things[i][j].level && bombList.at(k).centerX - 10 > things[i][j].locationX - 20 && bombList.at(k).centerX + 10 < things[i][j].locationX + 20 && bombList.at(k).centerY - 10 > things[i][j].locationY - 20 && bombList.at(k).centerY + 10 < things[i][j].locationY + 20) {
					things[i][j].exists = 0;
				}
			}
		}
	}
	//send the current point to the bitMapStirng function
	glColor3f(0, 0, 0);
	char a[8] = "Point: ";
	bitMapString(10, 10, a);
	char s[32];
	itoa(calculatePoint(), s, 10);
	bitMapString(60, 10, s);
	glFlush();
}

// Initialization routine.
void setup(void)
{
	glClearColor(0.0, 0.5, 0.2, 0.0);
}

// OpenGL window reshape routine.
void resize(int w, int h)
{
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0.0, 400.0, 0.0, 400.0, -1.0, 1.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	// Pass the size of the OpenGL window to globals.
	width = w;
	height = h;
}


static int erase = 0;
static int m[5][4] = { 0 };
void motion() { // move the things and bombs one step

	for (int i = 0; i < 5; i++) {
		for (int j = 0; j < 4; j++) {
			things[i][j].locationX += things[i][j].speedX;
			things[i][j].locationY += things[i][j].speedY;

		}
	}
	if (erase > 5) {
		for (int i = 0; i < bombList.size(); i++) {
			bombList.at(i).level++;
			if (bombList.at(i).level > 4)
				bombList.erase(bombList.begin() + i);
		}
		erase = 0;
	}

	erase++;

	for (int i = 0; i < 5; i++) {
		for (int j = 0; j < 4; j++) {
			if (things[i][j].exists == 0)
				m[i][j]++;

			if (m[i][j] > 5) {
				things[i][j].deleteThing = 1;
				gameOverFunction();
			}
		}
	}
}


void myTimerFnc(int a) {  // if the game is paused, do not move things and bombs. if s is pressed move the things and bombs one step

	if (s_pressed == 0) {


		if (!paused) {
			glutTimerFunc(30, myTimerFnc, 0);
			motion();
		}
	}
	else motion();

	glutPostRedisplay();

}

// Keyboard input processing routine.
void keyInput(unsigned char key, int x, int y)  // if p is pressed make the game paused. if s is pressed move the things and bombs one step
{
	switch (key)
	{
	case 'q':
		exit(0);
		break;
	case 's':
		s_pressed++;
		paused = true;
		myTimerFnc(0);
		break;
	case 'p':
		s_pressed = 0;
		if (paused)
			paused = false;
		else
			paused = true;
		myTimerFnc(0);
		break;
	default:
		break;
	}
}

void mouse(int button, int state, int x, int y)   // if the right button of the mouse is clicked make the game paused. if middle button of the mouse is pressed move the things and bombs one step
{
	switch (button) {
	case GLUT_LEFT_BUTTON: {
		leftMouseButtonClicked++;
		BOMB bom;
		bom.centerX = 400 * x / width;
		bom.centerY = 400 - (400 * y / height);
		bom.level = 0;
		bombList.push_back(bom);
		break;
	}
	case GLUT_RIGHT_BUTTON: {
		if(state == GLUT_UP) {
			;
		}
		else {
			s_pressed = 0;
			if (paused)
				paused = false;
			else
				paused = true;
			myTimerFnc(0);
		}
		break;
	}
	case GLUT_MIDDLE_BUTTON: {
		s_pressed++;
		paused = true;
		myTimerFnc(0);
		break;
	}
	}
	glutPostRedisplay();
}




// Main routine.
int main(int argc, char **argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
	glutInitWindowSize(400, 400);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("BOOM!");
	setup();
	glutDisplayFunc(drawScene);
	glutReshapeFunc(resize);
	glutKeyboardFunc(keyInput);
	glutMouseFunc(mouse);
	srand(time(NULL));
	createThings();
	myTimerFnc(0);
	glutMainLoop();
	return 0;
}

