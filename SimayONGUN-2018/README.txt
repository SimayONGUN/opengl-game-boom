Bu oyundaki ama�; en az say�da iyi objeyi(sar� renkli) vurarak, k�t� objeleri(mor renkli) yok etmektir.
�yi objeler 0, 1 ve 2. seviyelerde, k�t� objeler ise 3. ve 4. seviyelerde ya�arlar.
Objeler rastgele y�nlerde rastgele bir h�zla hareket ederler ve oyun penceresine �arpt�klar�nda y�n de�i�tirirler.
Her objenin rengi bulundu�u seviyeye g�re daha a��k ya da daha koyudur.
D���k seviyedeki objeler daha koyu g�z�k�r.
Objeleri yok etmek i�in farenizin sol tu�unu kullanarak istedi�iniz yere bomba b�rakabilirsiniz.
Vurdu�unuz objeler iyiyse eksi puan, k�t�yse art� puan al�rs�n�z.
Her vurdu�unuz obje i�in puanlar: Seviye 0: -4000, Seviye 1: -2000, Seviye 2:-1000, Seviye 3: +1000, Seviye 4: +2000 �eklinde verilmi�tir.
Bomba at�ld�ktan sonra bir objeye �arparsa, d��meye devam eder.
Oyunu oynad���n�z pencereyi b�y�t�p k���ltt���n�zde objeler ve bombalar da bu oranda b�y�r ya da k���l�r.
Klavyenizin 'p' tu�uyla ya da farenizin sa� tu�uyla oyunu durdurabilirsiniz.
Oyunu durdurdu�unuzda bomba atmaya devam edebilirsiniz fakat bu bombalar, 'p' tu�una ya da farenin sa� tu�una basarak oyunu tekrar devam ettrmedi�iniz s�rece d��meye ba�lamaz.
Klavyenizin 's' tu�uyla ya da farenizin orta tu�uyla oyunu single step modda �al��t�rabilirsiniz.
Single step modundan ��kmak i�in farenizin sa� tu�unu ya da klavyenizin 'p' tu�unu kullanabilirsiniz.
E�er oyundaki t�m k�t� objeleri yok ederseniz veya klavyenizin 'q' tu�una basarsan�z, oyun biter.